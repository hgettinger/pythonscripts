"""
METAMOVE
MetaMove.py moves image files IF they contain Model Metadata
It scans through directories in a range, then checks each
file in the directory. If the file is an image, and it has
metadata indiciating a camera Model, the image is moved into
a different directory.

But, Why?
When recovering images with PhotoRec, without knowing where
images are stored, you'll want to scan the entire drive.
This hopefully leads to lots of images being recovered.
This script will step through each of the directories that
PhotoRec generates, finds the images that were created
with a camera, and makes copies of them in a seperate directory.
"""


# import required module
import os
import PIL.Image
import filetype
import shutil
from alive_progress import alive_bar

#Directory where the 
directory = "C:\\Users\\example\\Pictures\\recovered\\recup_dir."
#NOTICE
#the script does not create the recovery directory
#make it yourself before running the script
recovery_directory= "C:\\Users\\example\\Pictures\\recovered\\Meta\\"
#assign directory

#alive_bar makes a nice progress bar that moves when a directory completes
with alive_bar(116) as bar:
    for i in range(1,117):
        directory = "C:\\Users\\example\\Pictures\\recovered\\recup_dir." + str(i)
     
        # iterate over files in
        # that directory
        for filename in os.listdir(directory):
            f = os.path.join(directory, filename)    
            
            #skip files that throw exceptions
            try:
                if filetype.is_image(f):
                    img = PIL.Image.open(f)
                    exif_data = {
                        PIL.ExifTags.TAGS[k]: v
                        for k, v in img.getexif().items()
                        if k in PIL.ExifTags.TAGS
                        }
                    if exif_data.get('Model') != None:
                        shutil.copy2(f, recovery_directory)

            except Exception:
                
                #write the file names of any files that were skipped
                exceptionlog = open(recovery_directory + "\\MetaLog.txt", "w")
                exceptionlog.write(f + '\n')
                exceptionlog.close()
                pass
    bar()