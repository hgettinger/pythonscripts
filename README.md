# PythonScripts

There's really not much to it, this is where I'm putting my Python scripts that I like enough to make public facing. Maybe they're useful to someone else, maybe I thought I might like to use them again.

## MetaMove.py

This script crawls through directories created by photorec to find image files that have camera model metadata, and copies those files into another folder.

### Requirements
**Please read the source code before running**

**Defining variables is required**

alive-progress (https://github.com/rsalmei/alive-progress)

Pillow (https://pillow.readthedocs.io/en/stable/)

``pip install alive-progress Pillow``